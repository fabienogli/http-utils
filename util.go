package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_ = json.NewEncoder(w).Encode(payload)
}

func IsDevelopmentMode() bool {
	return os.Getenv("APP_MODE") == "development"
}

func RespondWithError(w http.ResponseWriter, code int, err error) {
	var msg string
	if IsDevelopmentMode() {
		msg = err.Error()
	} else {
		msg = http.StatusText(code)
	}
	RespondWithJSON(w, code, map[string]string{"error": msg})

	go logError(code, err)
}

func RespondWithMsg(w http.ResponseWriter, code int, msg string) {
	RespondWithJSON(w, code, map[string]string{"message": msg})
}

func logError(code int, err error) {
	if code == http.StatusInternalServerError {
		log.Printf("[%d] : %s", http.StatusInternalServerError, err.Error())
	}
}
